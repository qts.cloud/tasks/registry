# Task Description

1. The task is to: [SOLVED HERE](helm/)
    - write the Kubernetes deployment manifests to run Docker registry in Kubernetes with at least the following:
      - deployment
      - service
      - persistent volume claim
      - garbage collect cron job,
      - ingress
      - secret (if needed)
    - Make sure Docker registry uses `Redis` for cache
    - You run `2 replicas` of Docker registry for redundancy.
    - Record any issues you encounter.
    - You’re also welcome to list improvement ideas for the service that are outside the scope of this assignment.
1. Optional: [SOLVED HERE](build/)
    - Write a script that implements Docker registry retention.
    - There are containers with various tags pushed to the Docker registry which runs on premises, e.g.:
      - app/calculator:v1
      - app/calculator:v2
      - app/calculator:latest
      - app/calculator:v5
      - app/jmeter:v2 etc...
    - There is a need to go through all of the repositories in the registry and retain `only the last X images`. We might need to keep 10 images of the app/calculator and 5 images of app/jmeter - the retention job should be able to work with different retention settings.
