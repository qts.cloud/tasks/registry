# Cleanup Docker Registry

## Informations

Python script meant to be run as CronJob.
What it does:

- collects informations on existing repositories and tags
- reads configuration from file
- remove old digests
- skips when multiple tags have same digest

## How to?

### Prequisites

1. Docker Engine [Install](https://docs.docker.com/desktop/mac/install/)
1. docker-compose (comes pre-installed with Docker Engine for Mac)

### Check & Update files

1. [Dockerfile](Dockerfile)
1. [docker-compose.yml](docker-compose.yml)

### Build the image

```sh
#? Build image with defined args in the docker-compose.yml.
#? NOTE: Image is already pre-built and available on dockerhub
#?      docker pull ibacalu/registry_clean:0.0.4
docker-compose build
```

### Push the image to registry

```sh
docker-compose push
```

### Docker Scan (trivy)

```sh
#? Install prequisites
brew install trivy

#? Scan Dockerfile
trivy conf --severity HIGH Dockerfile

#? Scan Built Image
trivy image --severity HIGH ibacalu/registry_clean:0.0.4
```

### Run image & Check Logs

```sh
docker-compose up
```

### See it in action

It is currently deployed in the helm chart and configured so that it runs before GarbageCollector.
Relevant manifests:

- [ConfigMap](../helm/registry/templates/configmap.yaml#registry_clean)
- [CronJob](../helm/registry/templates/cronjob.yaml#registry_clean)
- Configuration:

  ```yaml
  #? Docker Registry Cleanup
  cleanup:
    image:
      repository: ibacalu/registry_clean
      tag: 0.0.4
    config:
      repositories:
        ibacalu/alpine: 1
        ibacalu/ubuntu: 1
  ```

## Credits

- [How-to-maintain-a-private-docker-registry](https://janethavishka.medium.com/how-to-maintain-a-private-docker-registry-d4f3d291e7d5)
- [Registry API](https://docs.docker.com/registry/spec/api)
