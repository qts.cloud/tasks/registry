#!/usr/bin/env python

import sys
import logging
import yaml
import requests
from dateutil import parser
from decouple import config

CONFIG = {
    "config": config('CONFIG', default='/app/config.yaml', cast=str),
    "registry_url": config('DOCKER_REGISTRY_URL', default='http://registry:5000'),
    "history": config('DOCKER_REGISTRY_HISTORY', default=3, cast=int),
    "log_level": config('LOG_LEVEL', default='INFO', cast=str)
}


def curl(path, host=CONFIG["registry_url"], method='GET'):
    url = "{}/v2/{}".format(host, path)
    try:
        response = requests.request(method,
                                    url,
                                    headers={
                                        'Accept': 'application/vnd.docker.distribution.manifest.v2+json'
                                    },
                                    timeout=10)
    except Exception as e:
        logging.warning("[{}] Warn: {}".format(url, e))
        response = {}
    return response


def load_config(path=CONFIG['config']):
    '''
    # config.yaml
    repositories:
      ibacalu/alpine: 3
      ibacalu/ubuntu: 1
    '''
    with open(path, 'r') as file:
        try:
            config = yaml.safe_load(file)['repositories']
        except Exception as e:
            logging.warning(
                "[load_config] Warn: {}. Could not read file {}. Using defaults.".format(e, path))
            config = {}
        logging.info("Config Loaded: {}".format(config))
    return config


def main(config={}):
    config = load_config()

    # ? Get all repositories
    repositories = curl("_catalog").json().get("repositories", [])

    # ? Avoid empty repos
    if repositories is not None:
        for repo in repositories:
            # ? digest:date
            digests = {}
            # ? digest:tag
            data = {}

            if repo in config:
                keep = config[repo]
            else:
                keep = CONFIG['history']

            tags = curl("{}/tags/list".format(repo)).json().get("tags", [])

            # ? Had issues with no images in a repo
            if tags is not None:
                for tag in tags:
                    tag_info = curl("{}/manifests/{}".format(repo, tag))
                    tag_config_digest = tag_info.json().get("config", {}).get("digest")
                    tag_header_digest = tag_info.headers.get(
                        "Docker-Content-Digest")

                    tag_date_txt = curl(
                        "{}/blobs/{}".format(repo, tag_config_digest)).json().get("created")
                    tag_date = parser.parse(tag_date_txt)

                    logging.info("[{}:{}] Digest: {}, Date: {}".format(
                        repo, tag, tag_header_digest, tag_date_txt))

                    # ? Append to dict(digest:date)
                    digests[tag_header_digest] = tag_date
                    # ? Append to dict(digest:tag)
                    data[tag_header_digest] = tag

                # ? Get oldest images
                sorted_tuples = sorted(
                    digests.items(), key=lambda item: item[1], reverse=True)

                # ? Avoid potential issues where different images share a digest
                if len(sorted_tuples) > keep:
                    logging.info("[{}] Cleaning required".format(repo))
                    to_delete = sorted_tuples[keep:]
                    digests = {k: v for k, v in to_delete}
                    for digest in digests.keys():
                        logging.info("[{}:{}] Deleting: {}".format(
                            repo, data[digest], digest))
                        delete = curl(
                            "{}/manifests/{}".format(repo, digest), method='DELETE')
                        logging.info("[{}:{}] Status: {}".format(
                            repo, data[digest], delete.status_code))


if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s %(levelname)s %(message)s',
        level=CONFIG['log_level'],
        stream=sys.stdout)

    main()
